<?php

namespace App\Controller\Admin;

use App\Entity\Category;
use App\Form\CategoryType;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\String\Slugger\SluggerInterface;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

#[IsGranted('ROLE_ADMIN')]
#[Route('/admin', name: 'admin_')]
class CategoryController extends AbstractController
{
    #[Route('/category', name: 'app_category')]
    public function index(): Response
    {
        return $this->render('category/index.html.twig', [
            'controller_name' => 'CategoryController',
        ]);
    }
    #[Route('/categories', name: 'category_list')]
    public function listAction(ManagerRegistry $doctrine): Response
    {
        $categories = $doctrine->getRepository('App\Entity\Category')->findAll();
        return $this->render('/admin/category/index.html.twig', ['categories' => $categories]);
    }
    #[Route('/category/details/{id}', name: 'show_category')]
    public function detailsAction(ManagerRegistry $doctrine, $id): Response
    {
        $category = $doctrine
            ->getRepository('App\Entity\Category')
            ->find($id);
        return $this->render('/admin/category/details.html.twig', ['category' => $category]);
    }
    #[Route('/category/create', name: 'create_category', methods: ['GET', 'POST'])]
    public function createAction(ManagerRegistry $doctrine, Request $request, SluggerInterface $slugger, AuthorizationCheckerInterface $authorizationChecker)
    {
        $category = new Category();
        $form = $this->createForm(CategoryType::class, $category);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $em = $doctrine->getManager();
            $em->persist($category);
            $em->flush();

            $this->addFlash(
                'notice',
                'Product Added'
            );
            return $this->redirectToRoute('category_list');
        }
        //        if(false == $authorizationChecker->isGranted('ROLE_ADMIN'))
        //        {
        //            return $this->redirectToRoute('product_list');
        //        }
        return $this->renderForm('admin/category/create.html.twig', ['form' => $form]);
    }
    #[Route('/category/edit/{id}', name: 'edit_category')]
    public function editAction(ManagerRegistry $doctrine, $id, Request $request)
    {
        $em = $doctrine->getManager();
        $product = $em->getRepository('App\Entity\Category')->find($id);
        $form = $this->createForm(CategoryType::class, $product);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $em = $doctrine->getManager();
            $em->persist($product);
            $em->flush();

            $this->addFlash(
                'notice',
                'Category Edited'
            );
            return $this->redirectToRoute('category_list');
        }
        return $this->renderForm('admin/category/edit.html.twig', ['form' => $form]);
    }
    #[Route('/category/delete/{id}', name: 'delete_category')]
    public function deleteAction(ManagerRegistry $doctrine, $id)
    {
        $em = $doctrine->getManager();
        $category = $em->getRepository('App\Entity\Category')->find($id);
        $em->remove($category);
        $em->flush();

        $this->addFlash(
            'error',
            'Product deleted'
        );
        //        $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'User tried to access a page without having ROLE_ADMIN');
        return $this->redirectToRoute('category_list');
    }
}
