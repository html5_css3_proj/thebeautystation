<?php

namespace App\Controller\Admin;

use App\Entity\Product;
use App\Form\ProductType;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\String\Slugger\SluggerInterface;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

#[IsGranted('ROLE_ADMIN')]
#[Route('/admin', name: 'admin_')]
class ProductController extends AbstractController
{
    #[Route('/product', name: 'app_product')]
    public function index(): Response
    {
        return $this->render('admin/product/index.html.twig', [
            'controller_name' => 'ProductController',
        ]);
    }
    #[Route('/products', name: 'product_list')]
    public function listAction(ManagerRegistry $doctrine): Response
    {
        $products = $doctrine->getRepository('App\Entity\Product')->findAll();
        return $this->render('/admin/product/index.html.twig', ['products' => $products]);
    }
    #[Route('/product/details/{id}', name: 'show_product')]
    public function detailsAction(ManagerRegistry $doctrine, $id): Response
    {
        $products = $doctrine
            ->getRepository('App\Entity\Product')
            ->find($id);
        $categories = $doctrine
            ->getRepository('App\Entity\Category')
            ->findAll();
        return $this->render('/admin/product/details.html.twig', ['products' => $products, 'categories' => $categories]);
    }
    #[Route('/product/create', name: 'create_product', methods: ['GET', 'POST'])]
    public function createAction(ManagerRegistry $doctrine, Request $request, SluggerInterface $slugger, AuthorizationCheckerInterface $authorizationChecker)
    {
        $product = new Product();
        $form = $this->createForm(ProductType::class, $product);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $em = $doctrine->getManager();
            $em->persist($product);
            $em->flush();

            $id = $product->getId();

            $this->addFlash(
                'notice',
                'Product Added'
            );
            return $this->redirectToRoute('create_gallery', ['id' => $id]);
        }
        //        if(false == $authorizationChecker->isGranted('ROLE_ADMIN'))
        //        {
        //            return $this->redirectToRoute('product_list');
        //        }
        return $this->renderForm('admin/product/create.html.twig', ['form' => $form]);
    }
    #[Route('/product/edit/{id}', name: 'edit_product')]
    public function editAction(ManagerRegistry $doctrine, $id, Request $request)
    {
        $em = $doctrine->getManager();
        $product = $em->getRepository('App\Entity\Product')->find($id);
        $form = $this->createForm(ProductType::class, $product);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $em = $doctrine->getManager();
            $em->persist($product);
            $em->flush();

            $this->addFlash(
                'notice',
                'Product Edited'
            );
            return $this->redirectToRoute('product_list');
        }
        return $this->renderForm('admin/product/edit.html.twig', ['form' => $form, 'id' => $id]);
    }
    #[Route('/product/delete/{id}', name: 'delete_product')]
    public function deleteAction(ManagerRegistry $doctrine, $id)
    {
        $em = $doctrine->getManager();
        $product = $em->getRepository('App\Entity\Product')->find($id);
        $em->remove($product);
        $em->flush();

        $this->addFlash(
            'error',
            'Product deleted'
        );
        //        $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'User tried to access a page without having ROLE_ADMIN');
        return $this->redirectToRoute('product_list');
    }
}
