<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class AuthorizationController extends AbstractController
{
    #[Route('/authorization', name: 'app_authorization')]
    public function index(AuthorizationCheckerInterface $authorizationChecker): Response
    {
        if(true == $authorizationChecker->isGranted('ROLE_ADMIN'))
        {
            return $this->redirectToRoute('admin_app_dashboard');
        } else
        {

        }
    }
}
